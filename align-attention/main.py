import numpy as np
import functools


def best_path_recursive(attention):
    @functools.lru_cache
    def best_path_inner(i, j):
        if i == 0 or j == 0:
            return [], 0
        else:
            results = [
                best_path_inner(i,     j - 1),
                best_path_inner(i - 1, j    ),
                best_path_inner(i - 1, j - 1),
            ]
            path, value = max(results, key=lambda t: t[1])
            path_star = path + [(i - 1, j - 1)]
            value_star = value + attention[i - 1, j - 1]
            return path_star, value_star
    return best_path_inner(*attention.shape)


def best_path_iterative(attention):
    m, n = attention.shape
    grid = attention.copy()

    for i in range(m - 2, -1, -1):
        grid[i,n - 1] += grid[i+1, n - 1]

    for j in range(n - 2, -1, -1):
        grid[m - 1, j] += grid[m - 1, j+1]

    for i in range(m - 2, -1, -1):
        for j in range(n - 2, -1, -1):
            grid[i, j] += max(grid[i + 1, j], grid[i, j + 1])

    path = [(0,0)]
    i = j = 0
    while i < m and j < n:
        if i == m-1:
            j+=1
        elif j == n-1:
            i+=1
        elif grid[i+1][j] > grid[i][j+1]:
            i += 1
        else:
            j+=1
        if i < m and j < n:
            path.append((i,j))

    return path
    

if __name__ == "__main__":
    x = np.random.rand(14, 7)
    attention = np.exp(x)
    path_1, value_1 = best_path_recursive(attention)
    path_2          = best_path_iterative(attention)
    print(path_1)
    print(path_2)
