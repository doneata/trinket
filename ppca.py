#
# TODO
# - [ ] Try to replicate figure from "Don't blame the ELBO" paper
# - [ ] 
#

import pdb
import random

import streamlit as st
import numpy as np

from matplotlib import pyplot as plt
from sklearn.datasets import load_digits
from scipy.stats import multivariate_normal


def show1(ax, datum):
    ax.imshow(datum.reshape(8, 8), cmap="gray")
    ax.set_xticks([])
    ax.set_yticks([])


digits = load_digits()

fig, axs = plt.subplots(ncols=5, nrows=3)
for i, ax in enumerate(axs.flatten()):
    datum = random.choice(digits.data)
    show1(ax, datum)
fig.tight_layout()

st.markdown("## Random samples")
st.pyplot(fig)

M = 8
X = digits.data.T
μ = np.mean(X, axis=1, keepdims=True)
S = np.cov(X)

st.markdown("## Mean image")
fig, ax = plt.subplots()
show1(ax, μ.T)
col, *_ = st.columns(5)
col.pyplot(fig)

L, U = np.linalg.eig(S)

σ = np.sqrt(np.mean(L[M + 1 :]))
W = U[:, :M] @ np.diag(np.sqrt(L[:M] - σ ** 2))
# W = W[:, :M]

fig, axs = plt.subplots(ncols=4, nrows=2)
for i, ax in enumerate(axs.flatten()):
    show1(ax, W[:, i].T)
fig.tight_layout()

st.markdown("## Directions")
st.pyplot(fig)


def eval_log_likelihood(μ, W, σ):
    m = μ.T[0]
    C = W @ W.T + (σ ** 2) * np.eye(len(m))
    p = multivariate_normal(m, C)
    return np.mean(p.logpdf(X.T))


def show_log_likelihood(W):
    fig, ax = plt.subplots(M, M)

    def update(W, dx, r, dy, c):
        Wx = np.zeros_like(W)
        Wy = np.zeros_like(W)
        Wx[:, r] = dy * U[:, r]
        Wy[:, c] = dx * U[:, c]
        return W + Wx + Wy

    for r in range(M):
        for c in range(M):
            if c >= r:
                ax[r, c].set_visible(False)
            else:
                x, y = np.mgrid[-10:10:1.0, -10:10:1.0]
                pos = np.dstack((x, y))
                log_like = np.vstack(
                    [
                        [
                            eval_log_likelihood(μ, update(W, dx, r, dy, c), σ)
                            for dx, dy in row
                        ]
                        for row in pos
                    ]
                )
                ax[r, c].contourf(x, y, log_like)
                ax[r, c].set_yticklabels([])
                ax[r, c].set_xticklabels([])
            if r > c:
                ax[r, c].set_title(r"$u_{}$".format(c))
            if c == 0:
                ax[r, c].set_ylabel(r"$u_{}$".format(r))
    # fig.tight_layout()
    st.pyplot(fig)


st.markdown("## Visualize log-likelihood")
st.markdown("Log likelihood at optimum: {:f}".format(eval_log_likelihood(μ, W, σ)))
show_log_likelihood(W)

st.markdown("Log likelihood at stationary point")
WW = W.copy()
WW[:, 2] = 0
show_log_likelihood(WW)

st.markdown("Log likelihood at zeros")
show_log_likelihood(np.zeros_like(W))
