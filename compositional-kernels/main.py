"""This snippet verifies my understanding of why kernels are compositional
under addition and multiplication. The motivation comes from homework 7 from
CSC411, by Roger Grosse:

http://www.cs.toronto.edu/~rgrosse/courses/csc411_f18/homeworks/hw7/hw7.pdf

"""

import pdb

import numpy as np
np.random.seed(1337)

from sklearn.preprocessing import PolynomialFeatures


def feature_to_kernel(phi):
    def kernel(x1, x2):
        return np.squeeze(phi(x1).T @ phi(x2))

    return kernel


def linear(x):
    return x


def get_poly(degree):
    # Polynomial features
    poly_sk = PolynomialFeatures(degree)
    poly = lambda x: poly_sk.fit_transform(x.T).T
    return poly


def sum_kernel(k1, k2):
    def kernel(x1, x2):
        return k1(x1, x2) + k2(x1, x2)
    return kernel


def sum_features(phi1, phi2):
    def feature(x):
        return np.vstack((phi1(x), phi2(x)))
    return feature


def product_kernel(k1, k2):
    def kernel(x1, x2):
        return k1(x1, x2) * k2(x1, x2)
    return kernel


def product_features(phi1, phi2):
    def feature(x):
        emb1 = phi1(x)
        emb2 = phi2(x)
        D1, _ = np.shape(emb1)
        D2, _ = np.shape(emb2)
        get_i = lambda k: k // D2
        get_j = lambda k: k % D2
        emb = [emb1[get_i(k)] * emb2[get_j(k)] for k in range(D1 * D2)]
        return np.vstack(emb)
    return feature


def main():
    D = 10
    x1 = np.random.randn(D, 1)
    x2 = np.random.randn(D, 1)

    print("This script checks numerically that the composite kernels are ")
    print("equal to the inner product of suitable feature maps.")
    print()

    print("We use the following kernels")
    print("- k_1 → linear kernel")
    print("- k_2 → quadratic kernel")
    print()

    phi1 = linear
    phi2 = get_poly(2)

    k1 = feature_to_kernel(phi1)
    k2 = feature_to_kernel(phi2)

    print("The sum of two kernels is a kernel")
    ks1 = sum_kernel(k1, k2)
    ks2 = feature_to_kernel(sum_features(phi1, phi2))
    print("k_s(x, x')           = ", ks1(x1, x2))
    print("phi_s(x)^T phi_s(x') = ", ks2(x1, x2))
    print()

    print("The product of two kernels is a kernel")
    ks1 = product_kernel(k1, k2)
    ks2 = feature_to_kernel(product_features(phi1, phi2))
    print("k_p(x, x')           = ", ks1(x1, x2))
    print("phi_p(x)^T phi_p(x') = ", ks2(x1, x2))


if __name__ == "__main__":
    main()
