Table of contents

- `mixture-of-bernoullis` being image completion using mixture of Bernoullis for image completion. The code is based on an assignment by Roger Grosse for the CSC411 class.
- `compositional-kernels` being a numerical check that the sum and product of kernels can be written as inner product of appropiate feature maps. The code is motivated by homework 7 from the CSC411 class.
- `model-agnostic-meta-learning` being a self-containted implementation of the gradient-based meta-learning algorithm, MAML. The code is based on an assignment from the CSC421 class. The file `maml_func.py` contains a more functional implementation of the code, which is arguably easier to understand.
- `variational-autoencoders` being a play on the work of Kingma and Welling. The notebook is based on assignment 3 from the CSC412 class from spring 2019.
- `pixel-cnn` being a toy implementation of an auto-regressive models based on masked convolutions. Inspired by the first assignment from [CS294-158 Deep Unsupervised Learning](https://sites.google.com/view/berkeley-cs294-158-sp19/home).
- `flows` being an intro to generative flow models (CDF-based and real NVP). Inspired by the second assignment from [CS294-158 Deep Unsupervised Learning](https://sites.google.com/view/berkeley-cs294-158-sp19/home).
- `gan` being a few exercises on generative adversarial networks (GANs) from an assignment from Stanford's CS236.
- `cs330-hw1-mann` being the first homework exercise from Stanford's [CS330 course](https://cs330.stanford.edu/) on memory-augmented neural networks.
- `align-attention` being a snippet to use dynamic programming to generate alignments based on attention.
- `energy-based-models` being a toy example on how to train an EBM with score matching.
- `sensitivity-analysis.py` shows that the bound on the relative error of a linear system is tight.
- `auto-diff-cat` attempts at an idiomatic translation of Conal Elliott's "Essence of automatic differentiation" paper in Pythondifferentiation" paper in Python.
