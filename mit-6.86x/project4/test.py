import pdb
import sys
import numpy as np
import em
import common

todo = sys.argv[1]

if todo == 'netflix':
    K = 12
    SEEDS = range(5)
    X = np.loadtxt("netflix_incomplete.txt")
    X_gold = np.loadtxt("netflix_complete.txt")
elif todo == 'test':
    K = 4
    SEEDS = range(1)
    X = np.loadtxt("test_incomplete.txt")
    X_gold = np.loadtxt("test_complete.txt")

n, d = X.shape
# mixture, post = common.init(X, K, seed)
# mixture, post, loglike = em.run(X, mixture, post)
mixture, post, loglike = max([em.run(X, *common.init(X, K, seed)) for seed in SEEDS], key=lambda t: t[2])
print(loglike)
X_pred = em.fill_matrix(X, mixture)
print(common.rmse(X_gold, X_pred))
