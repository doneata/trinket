"""Mixture model for matrix completion"""
from typing import Tuple
import pdb
import numpy as np
from scipy.special import logsumexp
from common import GaussianMixture


def estep(X: np.ndarray, mixture: GaussianMixture) -> Tuple[np.ndarray, float]:
    """E-step: Softly assigns each datapoint to a gaussian component

    Args:
        X: (n, d) array holding the data, with incomplete entries (set to 0)
        mixture: the current gaussian mixture

    Returns:
        np.ndarray: (n, K) array holding the soft counts
            for all components for all examples
        float: log-likelihood of the assignment

    """
    N, _ = X.shape
    K, _ = mixture.mu.shape
    f = np.zeros((N, K))
    def log_pdf(x, m, v):
        D, = x.shape
        return - D / 2 * np.log(2 * np.pi * v) - ((x - m) ** 2).sum() / (2 * v)
    for u in range(N):
        for i in range(K):
            idxs = X[u] != 0
            f[u, i] = (
                np.log(mixture.p[i]) +
                log_pdf(X[u, idxs], mixture.mu[i, idxs], mixture.var[i])
            )
    m = np.max(f, axis=1, keepdims=True)
    Z = m + np.log(np.sum(np.exp(f - m), axis=1, keepdims=True))
    log_like = Z.sum()
    log_post = f - Z
    post = np.exp(log_post)
    return post, log_like


def mstep(X: np.ndarray, post: np.ndarray, mixture: GaussianMixture,
          min_variance: float = .25) -> GaussianMixture:
    """M-step: Updates the gaussian mixture by maximizing the log-likelihood
    of the weighted dataset

    Args:
        X: (n, d) array holding the data, with incomplete entries (set to 0)
        post: (n, K) array holding the soft counts
            for all components for all examples
        mixture: the current gaussian mixture
        min_variance: the minimum variance for each gaussian

    Returns:
        GaussianMixture: the new gaussian mixture
    """
    N, D = X.shape
    _, K = post.shape
    # weights
    p = np.array([post[:, k].sum() / N for k in range(K)])
    # means
    m = np.zeros(mixture.mu.shape)
    for j in range(K):
        for l in range(D):
            mask = X[:, l] != 0
            num = post[:, j] * mask @ X[:, l]
            den = post[:, j] @ mask
            m[j, l] = num / den if den >= 1 else mixture.mu[j, l]
    # variances
    v = np.zeros(mixture.var.shape)
    nn = np.sum(X != 0, axis=1)
    for j in range(K):
        mm = np.repeat(m[j][np.newaxis], N, 0)
        mm[X == 0] = 0
        num = post[:, j] @ np.sum((X - mm) ** 2, axis=1)
        den = post[:, j] @ nn
        v[j] = np.maximum(num / den, min_variance)
    return GaussianMixture(m, v, p)


def run(X: np.ndarray, mixture: GaussianMixture,
        post: np.ndarray) -> Tuple[GaussianMixture, np.ndarray, float]:
    """Runs the mixture model

    Args:
        X: (n, d) array holding the data
        post: (n, K) array holding the soft counts
            for all components for all examples

    Returns:
        GaussianMixture: the new gaussian mixture
        np.ndarray: (n, K) array holding the soft counts
            for all components for all examples
        float: log-likelihood of the current assignment
    """
    prev_loglike = None
    loglike = None
    while prev_loglike is None or loglike - prev_loglike > 1e-6 * abs(loglike):
        prev_loglike = loglike
        post, loglike = estep(X, mixture)
        mixture = mstep(X, post, mixture)
        print("...", loglike)
    print()
    return mixture, post, loglike


def fill_matrix(X: np.ndarray, mixture: GaussianMixture) -> np.ndarray:
    """Fills an incomplete matrix according to a mixture model

    Args:
        X: (n, d) array of incomplete data (incomplete entries =0)
        mixture: a mixture of gaussians

    Returns
        np.ndarray: a (n, d) array with completed data
    """
    N, D = X.shape
    K, _ = mixture.mu.shape

    def compute_post():
        f = np.zeros((N, K))
        def log_pdf(x, m, v):
            D, = x.shape
            return - D / 2 * np.log(2 * np.pi * v) - ((x - m) ** 2).sum() / (2 * v)
        for u in range(N):
            for i in range(K):
                idxs = X[u] != 0
                f[u, i] = (
                    np.log(mixture.p[i]) +
                    log_pdf(X[u, idxs], mixture.mu[i, idxs], mixture.var[i])
                )
        m = np.max(f, axis=1, keepdims=True)
        Z = m + np.log(np.sum(np.exp(f - m), axis=1, keepdims=True))
        log_post = f - Z
        post = np.exp(log_post)
        return post

    Xout = X.copy()
    post0 = compute_post()
    post1 = post0 / mixture.var[np.newaxis]
    Xout[X == 0] = (post0 @ mixture.mu / np.sum(post0, axis=1, keepdims=True))[X == 0]
    return Xout
