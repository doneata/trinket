"""Mixture model using EM"""
from typing import Tuple
import itertools
import pdb
import numpy as np
import sys
from common import GaussianMixture, bic, init, plot


def estep(X: np.ndarray, mixture: GaussianMixture) -> Tuple[np.ndarray, float]:
    """E-step: Softly assigns each datapoint to a gaussian component

    Args:
        X: (n, d) array holding the data
        mixture: the current gaussian mixture

    Returns:
        np.ndarray: (n, K) array holding the soft counts
            for all components for all examples
        float: log-likelihood of the assignment
    """
    N, _ = X.shape
    K, D = mixture.mu.shape
    post = np.zeros((N, K))

    for n, k in itertools.product(range(N), range(K)):
        diff = np.sum((X[n] - mixture.mu[k]) ** 2)
        post[n, k] = mixture.p[k] * np.exp(- diff / (2 * mixture.var[k])) / ((2 * np.pi * mixture.var[k]) ** (D / 2))

    # Normalize
    norm = post.sum(axis=1)
    post = post / norm[:, np.newaxis]
    loglike = np.log(norm).sum()

    return post, loglike


def mstep(X: np.ndarray, post: np.ndarray) -> GaussianMixture:
    """M-step: Updates the gaussian mixture by maximizing the log-likelihood
    of the weighted dataset

    Args:
        X: (n, d) array holding the data
        post: (n, K) array holding the soft counts
            for all components for all examples

    Returns:
        GaussianMixture: the new gaussian mixture
    """
    N, D = X.shape
    _, K = post.shape
    p = np.array([post[:, k].sum() / N for k in range(K)])
    m = np.array([post[:, k] @ X / post[:, k].sum() for k in range(K)])
    v = np.array([post[:, k] @ ((X - m[k]) ** 2).sum(axis=1) / (D * post[:, k].sum()) for k in range(K)])
    return GaussianMixture(m, v, p)


def run(X: np.ndarray, mixture: GaussianMixture,
        post: np.ndarray) -> Tuple[GaussianMixture, np.ndarray, float]:
    """Runs the mixture model

    Args:
        X: (n, d) array holding the data
        post: (n, K) array holding the soft counts
            for all components for all examples

    Returns:
        GaussianMixture: the new gaussian mixture
        np.ndarray: (n, K) array holding the soft counts
            for all components for all examples
        float: log-likelihood of the current assignment
    """
    prev_loglike = None
    loglike = None
    while prev_loglike is None or loglike - prev_loglike > 1e-6 * abs(loglike):
        prev_loglike = loglike
        post, loglike = estep(X, mixture)
        mixture = mstep(X, post)
    return mixture, post, loglike


def main(X, K, seed):
    mixture, post = init(X, K, seed)
    return run(X, mixture, post)


if __name__ == "__main__":
    SEEDS = [0, 1, 2, 3, 4]
    K = int(sys.argv[1])
    X = np.loadtxt("toy_data.txt")
    results = [main(X, K, seed) for seed in SEEDS]
    mixture, post, loglike = max(results, key=lambda x: bic(X, x[0], x[2]))
    # print(mixture)
    print(loglike)
    print(bic(X, mixture, loglike))
    print()
    # plot(X, mixture, post, f"{K}")
