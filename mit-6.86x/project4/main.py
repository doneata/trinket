import pdb
import sys

import numpy as np
import kmeans
import common
import naive_em
import em


Ks = [1, 2, 3, 4]
SEEDS = [0, 1, 2, 3, 4]
X = np.loadtxt("toy_data.txt")


def main(K, seed):
    mixture, post = common.init(X, K, seed)
    return kmeans.run(X, mixture, post)


if __name__ == "__main__":
    K = int(sys.argv[1])
    assert K in Ks
    results = [main(K, seed) for seed in SEEDS]
    mixture, post, cost = min(results, key=lambda x: x[2])
    print(mixture)
    print(cost)
    common.plot(X, mixture, post, f"{K}")
