# Assignment 1 from CS294-158 Deep Unsupervised Learning course at Berkeley
# Link to download the data: https://drive.google.com/uc?id=1hm077GxmIBP-foHxiPtTxSNy371yowk2&export=download

# TODO
# - [ ] Improve selection of CUDA vs. CPU
# - [ ] Evaluate on test data every n iterations

import itertools
import os
import pdb
import pickle
import random
import sys

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

from torch.distributions import Categorical
from torch.nn.modules.normalization import LayerNorm

from matplotlib import pyplot as plt


SEED = 1337
random.seed(SEED)
np.random.seed(SEED)
torch.manual_seed(0)

CRITERION = nn.NLLLoss()


def load_data():
    with open("mnist-hw1.pkl", "rb") as f:
        data = pickle.load(f)
    data = {
        k: torch.tensor(data[k]).permute(0, 3, 1, 2).type(torch.FloatTensor)
        for k in ["train", "test"]
    }
    tr_data = data["train"]
    te_data = data["test"]
    return tr_data, te_data


def load_model():
    model = PixelCNN1(128)
    model.load_state_dict(torch.load('model.pt'))
    return model.cuda()


def get_padding(padding_type, kernel_size):
    assert padding_type in ["SAME", "VALID"], f"Unknown padding type {padding_type}"
    if padding_type == "SAME":
        return tuple((k - 1) // 2 for k in kernel_size)
    return tuple(0 for _ in kernel_size)


def get_mask(type_, kernel_size):
    w, h = kernel_size
    mask = torch.ones(w, h)

    rc = h // 2
    cc = w // 2

    if type_ in "aA":
        for r, c in itertools.product(range(h), range(w)):
            if r > rc or r == rc and c >= cc:
                mask[r, c] = 0
    elif type_ in "bB":
        for r, c in itertools.product(range(h), range(w)):
            if r > rc or r == rc and c > cc:
                mask[r, c] = 0
    else:
        assert False, f"Unknown mask type {type_}"

    return mask.cuda()


class Conv2dMasked(nn.Conv2d):
    def __init__(self, in_channels, out_channels, kernel_size, mask_type, **kwargs):
        super(Conv2dMasked, self).__init__(
            in_channels, out_channels, kernel_size, **kwargs
        )
        self.mask = get_mask(mask_type, kernel_size)
        self.conv2d = lambda i, w: F.conv2d(i, w, **kwargs)

    def forward(self, x):
        mask = self.mask
        mask = mask[None, None]  # mask is 2d, unsqueeze for broadcast
        return self.conv2d(x, self.weight * mask)


class ResidualBlock(nn.Module):
    def __init__(self, h: int):
        super(ResidualBlock, self).__init__()
        self.block = nn.Sequential(
            nn.ReLU(),
            nn.Conv2d(2 * h, h, (1, 1)),
            nn.ReLU(),
            Conv2dMasked(h, h, (3, 3), "b", padding=get_padding("SAME", (3, 3))),
            nn.ReLU(),
            nn.Conv2d(h, 2 * h, (1, 1)),
        )

    def forward(self, x):
        return x + self.block(x)


class PixelCNN1(nn.Module):
    def __init__(self, h: int):
        super(PixelCNN1, self).__init__()
        self.layer_norm = LayerNorm((3, 28, 28))
        self.conv_7x7 = Conv2dMasked(
            3, 2 * h, (7, 7), "a", padding=get_padding("SAME", (7, 7))
        )
        self.residual = nn.Sequential(*[ResidualBlock(h) for _ in range(12)])
        self.conv_out = [
            nn.Sequential(
                nn.ReLU(),
                nn.Conv2d(2 * h, 32, (1, 1)),
                nn.ReLU(),
                nn.Conv2d(32, 4, (1, 1)),
                nn.LogSoftmax(dim=1),
            ).cuda()
            for _ in range(3)
        ]

    def forward(self, x):
        x = self.layer_norm(x)
        x = self.conv_7x7(x)
        x = self.residual(x)
        r, g, b = [f(x) for f in self.conv_out]
        return r, g, b


def evaluate1(output, input_, detach=False):
    r, g, b = output
    input_long = input_.type(torch.LongTensor)
    input_long = input_long.cuda()
    loss = (
        CRITERION(r, input_long[:, 0]) +
        CRITERION(g, input_long[:, 1]) +
        CRITERION(b, input_long[:, 2])
    )
    loss = loss / 3
    if detach:
        loss = loss.cpu().detach().numpy()
    return loss


def grad_input():
    model = PixelCNN1(128)
    model.eval()

    input = torch.randint(low=0, high=3, size=(1, 3, 28, 28))
    input = input.type(torch.FloatTensor)
    input.requires_grad = True

    r, _, _ = model(input)
    i = input.type(torch.LongTensor)

    loss = CRITERION(r[:, :, 14, 14], i[:, 0, 14, 14])
    loss.backward()

    grad = input.grad.data
    grad = grad.abs()
    grad = grad.squeeze()
    grad = grad.max(0)[0]

    plt.imshow(grad)
    plt.savefig('/tmp/o.png')


def train(to_resume=True):
    tr_data, te_data = load_data()

    batch_size = 128
    n_tr = len(tr_data)

    if to_resume and os.path.exists('model.pt'):
        model = load_model()
    else:
        model = PixelCNN1(128)
        model.cuda()

    print(list(model.parameters())[0][0, 0, :10])
    optimizer = optim.Adam(model.parameters(), lr=0.001)

    def train1(batch):
        batch = batch.cuda()
        optimizer.zero_grad()
        output = model(batch)
        loss = evaluate1(output, batch)
        loss.backward()
        optimizer.step()
        return loss.cpu().detach().numpy()

    for i in range(1024):
        idxs = torch.randint(high=n_tr, size=(batch_size,))
        batch = tr_data[idxs]
        tr_loss = train1(batch)
        print(i, tr_loss / np.log(2), end=" ")
        if i % 16 == 0:
            va_batch = te_data[:32].cuda()
            va_loss = evaluate1(model(va_batch), va_batch, detach=True)
            print(va_loss / np.log(2))
        else:
            print()
        # if i == 100:
        #     pdb.set_trace()
        # va_loss = evaluate(model(), va_data, detach=True)

    print(list(model.parameters())[0][0, 0, :10])
    torch.save(model.state_dict(), 'model.pt')


def evaluate():
    _, data = load_data()
    N = len(data)
    B = 128

    model = load_model()
    model.eval()

    idxs = torch.randint(high=N, size=(B,))
    batch = data[idxs]
    preds = model(batch.cuda())
    loss = evaluate1(preds, batch, detach=True)
    print(loss / np.log(2))


def sample():
    B, C, H, W = 64, 3, 28, 28

    batch = torch.zeros(B, C, H, W)
    batch = batch.cuda()

    model = load_model()
    model.eval()

    for i, j in itertools.product(range(H), range(W)):
        print(i, j)
        out = model(batch)
        for c in range(C):
            probas = out[c][:, :, i, j].exp()
            categorical = Categorical(probas)
            batch[:, c, i, j] = categorical.sample()

    torch.save(batch, 'samples.pt')
    
    batch = batch.cpu().numpy()
    batch = batch.transpose(0, 2, 3, 1)
    batch = batch / 3

    for i in range(B):
        plt.imshow(batch[i]) 
        plt.savefig(f'samples/{i:03d}.png')


def main():
    task = sys.argv[1]
    if task == 'train':
        train()
    elif task == 'evaluate':
        evaluate()
    elif task == 'grad':
        grad_input()
    elif task == 'sample':
        sample()
    else:
        pass


if __name__ == "__main__":
    main()
