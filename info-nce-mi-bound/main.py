import pdb

import numpy as np
import torch

from torch.distributions.multivariate_normal import MultivariateNormal
from torch.distributions.normal import Normal

σ1 = 1.0
σ2 = 1.0
ρ = 0.8

D = 2

μ = np.array([0.0, 1.0])
Σ = np.array([[σ1 ** 2, ρ * σ1 * σ2], [ρ * σ1 * σ2, σ2 ** 2]])


def conditional(y):
    μ_x_given_y = μ[0] + Σ[0, 1] * (y - μ[1]) / Σ[1, 1]
    Σ_x_given_y = Σ[0, 0] - Σ[0, 1] ** 2 / Σ[1, 1]
    return torch.tensor([μ_x_given_y]), torch.tensor([Σ_x_given_y])


def marginal_x():
    μ_x = μ[0]
    Σ_x = Σ[0, 0]
    return torch.tensor([μ_x]), torch.tensor([Σ_x])


def marginal_y():
    μ_y = μ[1]
    Σ_y = Σ[1, 1]
    return torch.tensor([μ_y]), torch.tensor([Σ_y])


def mutual_information():
    return -0.5 * np.log(1 - ρ ** 2)


def mutual_information_sample():
    p_x = Normal(*marginal_x())
    p_y = Normal(*marginal_y())
    p_xy = MultivariateNormal(torch.tensor(μ), torch.tensor(Σ))
    samples = p_xy.sample([2 ** 16])
    return torch.mean(
        p_xy.log_prob(samples).float()
        - p_x.log_prob(samples[:, 0].float())
        - p_y.log_prob(samples[:, 1].float())
    ).item()


def info_nce(n, y):
    p_x_given_y = Normal(*conditional(y))
    p_x = Normal(*marginal_x())
    pos_samples = p_x_given_y.sample([1])
    neg_samples = p_x.sample([n - 1])
    samples = torch.cat((pos_samples, neg_samples), dim=0).squeeze(1)
    ratios = (p_x_given_y.log_prob(samples) - p_x.log_prob(samples)).exp()
    return -torch.mean(torch.log(ratios[0] / torch.sum(ratios)))


def compute_lower_bound_1(n):
    y = σ2 * np.random.randn() + μ[1]
    log_n = torch.log(torch.tensor(n).float())
    return log_n - info_nce(n, y)


num_repeats = 2 * 12
print("mutual information (closed form) =", mutual_information())
print("mutual information (sampled)     =", mutual_information_sample())
for i in range(1, 16):
    n = 2 ** i
    lower_bound = np.mean([compute_lower_bound_1(n) for _ in range(128)])
    print("lower bound (n = {:5d})          = {}".format(n, lower_bound))
