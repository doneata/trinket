import argparse
import pdb

import numpy as np
import torch
import tqdm

from matplotlib import pyplot as plt

from pprint import pprint

from torchvision import datasets, transforms

from codebase import utils as ut
from codebase.models.vae import VAE
from codebase.train import train


parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--z',         type=int, default=10,      help="Number of latent dimensions")
parser.add_argument('--iter_max',  type=int, default=20000,   help="Number of training iterations")
parser.add_argument('--iter_save', type=int, default=10000,   help="Save model every n iterations")
parser.add_argument('--run',       type=int, default=0,       help="Run ID. In case you want to run replicates")
parser.add_argument('--todo',      type=str, default="train", help="What task to do")
parser.add_argument('--train',     type=int, default=1,       help="Flag for training")
args = parser.parse_args()
layout = [
    ('model={:s}',  'vae'),
    ('z={:02d}',  args.z),
    ('run={:04d}', args.run)
]
model_name = '_'.join([t.format(v) for (t, v) in layout])
pprint(vars(args))
print('Model name:', model_name)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
train_loader, labeled_subset, _ = ut.get_mnist_data(device, use_test_subset=True)
vae = VAE(z_dim=args.z, name=model_name).to(device)

if args.todo == "train":
    writer = ut.prepare_writer(model_name, overwrite_existing=True)
    train(model=vae,
          train_loader=train_loader,
          labeled_subset=labeled_subset,
          device=device,
          tqdm=tqdm.tqdm,
          writer=writer,
          iter_max=args.iter_max,
          iter_save=args.iter_save)
    ut.evaluate_lower_bound(vae, labeled_subset, run_iwae=args.train == 2)
elif args.todo == "test":
    ut.load_model_by_name(vae, global_step=args.iter_max)
    ut.evaluate_lower_bound(vae, labeled_subset, run_iwae=True)
elif args.todo in {"sample-p", "sample-x"}:
    # Load model
    ut.load_model_by_name(vae, global_step=args.iter_max)
    # Sample data
    if args.todo == "sample-p":
        # Generates probability over each pixel
        zs = vae.sample_z(200)
        xs = vae.dec.decode(zs)
        xs = torch.sigmoid(xs)
    elif args.todo == "sample-x":
        # Generates pixel values
        xs = vae.sample_x(200)
    else:
        assert False, f"Unknown operation: {args.todo}"
    # Prepare data for ploting
    xs = xs.reshape(200, 28, 28).reshape(10, 20, 28, 28)
    xs = xs.permute(0, 2, 1, 3).reshape(10 * 28, 20 * 28)
    xs = xs.detach().cpu().numpy()
    # Plot samples
    plt_path = f"output/{args.todo}_{model_name}.png"
    plt.imshow(xs, cmap='gray')
    plt.axis('off')
    plt.savefig(plt_path, dpi=200)
    print(f"Plot saved to {plt_path}")
