import pdb
import torch
from codebase import utils as ut
from codebase.models import nns
from codebase.models.nns import v1, v2
from torch import nn
from torch.nn import functional as F

class VAE(nn.Module):
    def __init__(self, nn='v1', name='vae', z_dim=2):
        super().__init__()
        self.name = name
        self.z_dim = z_dim
        # Small note: unfortunate name clash with torch.nn
        # nn here refers to the specific architecture file found in
        # codebase/models/nns/*.py
        nn = getattr(nns, nn)
        self.enc = nn.Encoder(self.z_dim)
        self.dec = nn.Decoder(self.z_dim)

        # Set prior as fixed parameter attached to Module
        self.z_prior_m = torch.nn.Parameter(torch.zeros(1), requires_grad=False)
        self.z_prior_v = torch.nn.Parameter(torch.ones(1), requires_grad=False)
        self.z_prior = (self.z_prior_m, self.z_prior_v)

    def negative_elbo_bound(self, x):
        """
        Computes the Evidence Lower Bound, KL and, Reconstruction costs

        Args:
            x: tensor: (batch, dim): Observations

        Returns:
            nelbo: tensor: (): Negative evidence lower bound
            kl: tensor: (): ELBO KL divergence to prior
            rec: tensor: (): ELBO Reconstruction term
        """
        ################################################################################
        # Compute negative Evidence Lower Bound and its KL and Rec decomposition
        # Note that nelbo = kl + rec
        ################################################################################
        m, v = self.enc.encode(x)
        z = ut.sample_gaussian(m, v)
        log_p = self.dec.decode(z)
        rec = ut.log_bernoulli_with_logits(x, log_p)
        rec = rec.mean()

        kl = ut.kl_normal(m, v, self.z_prior_m, self.z_prior_v)
        kl = kl.mean()

        elbo = rec - kl
        nelbo = - elbo

        return nelbo, kl, rec

    def negative_iwae_bound(self, x, iw):
        """
        Computes the Importance Weighted Autoencoder Bound
        Additionally, we also compute the ELBO KL and reconstruction terms

        Args:
            x: tensor: (batch, dim): Observations
            iw: int: (): Number of importance weighted samples

        Returns:
            niwae: tensor: (): Negative IWAE bound
            kl: tensor: (): ELBO KL divergence to prior
            rec: tensor: (): ELBO Reconstruction term
        """
        ################################################################################
        # TODO: Modify/complete the code here
        # Compute niwae (negative IWAE) with iw importance samples, and the KL
        # and Rec decomposition of the Evidence Lower Bound
        #
        # Outputs should all be scalar
        ################################################################################
        m, v = self.enc.encode(x)
        m1 = ut.duplicate(m, iw)
        v1 = ut.duplicate(v, iw)
        x1 = ut.duplicate(x, iw)
        z = ut.sample_gaussian(m1, v1)
        log_p = self.dec.decode(z)

        log_p_x_given_z = ut.log_bernoulli_with_logits(x1, log_p)
        log_p_z = ut.log_normal(z, self.z_prior_m, self.z_prior_v)
        log_q_z_given_x = ut.log_normal(z, m1, v1)

        log_r = log_p_x_given_z + log_p_z - log_q_z_given_x
        log_r = log_r.view(iw, x.shape[0])
        niwae = - ut.log_mean_exp(log_r, dim=0).mean()

        rec = log_p_x_given_z.mean()
        kl = ut.kl_normal(m1, v1, self.z_prior_m, self.z_prior_v).mean()
        ################################################################################
        # End of code modification
        ################################################################################
        return niwae, kl, rec

    def loss(self, x):
        nelbo, kl, rec = self.negative_elbo_bound(x)
        loss = nelbo

        summaries = dict((
            ('train/loss', nelbo),
            ('gen/elbo', -nelbo),
            ('gen/kl_z', kl),
            ('gen/rec', rec),
        ))

        return loss, summaries

    def sample_sigmoid(self, batch):
        z = self.sample_z(batch)
        return self.compute_sigmoid_given(z)

    def compute_sigmoid_given(self, z):
        logits = self.dec.decode(z)
        return torch.sigmoid(logits)

    def sample_z(self, batch):
        return ut.sample_gaussian(
            self.z_prior[0].expand(batch, self.z_dim),
            self.z_prior[1].expand(batch, self.z_dim))

    def sample_x(self, batch):
        z = self.sample_z(batch)
        return self.sample_x_given(z)

    def sample_x_given(self, z):
        return torch.bernoulli(self.compute_sigmoid_given(z))
