# Example taken from:
# https://twitter.com/alfcnz/status/1633220788546404352
# https://twitter.com/gucamporese/status/1633507155629228032

import math
import pdb
import torch

from torch import nn
from torch.nn import functional as F
from torch.distributions.multivariate_normal import MultivariateNormal

from matplotlib import pyplot as plt
from tqdm import tqdm

import streamlit as st
import numpy as np

N_PTS = 100
ITERS = 30_000
# ITERS = 3_000
B = 64
DEVICE = "cpu"
D = 2
M = 2


def load_spiral_data():
    θ = torch.linspace(0, 3 * torch.pi, N_PTS)
    r = torch.linspace(0.25, 1, N_PTS)
    x = r * torch.cos(θ)
    y = r * torch.sin(θ)
    return torch.stack([x, y], dim=1)


data = load_spiral_data()
fig, ax = plt.subplots()
ax.plot(data[:, 0], data[:, 1], lw=2)
st.pyplot(fig)

XMIN, YMIN = data.min(dim=0).values
XMAX, YMAX = data.max(dim=0).values


def plot_rec(x, x_rec):
    fig, axs = plt.subplots(figsize=(8, 4), ncols=2)
    for ax in axs:
        ax.set_xlim([XMIN, XMAX])
        ax.set_ylim([YMIN, YMAX])
    c = np.arange(len(x))
    axs[0].scatter(x[:, 0], x[:, 1], lw=2, c=c)
    axs[1].scatter(x_rec[:, 0], x_rec[:, 1], lw=2, c=c)
    st.pyplot(fig)


class VAE(nn.Module):
    def __init__(self):
        super().__init__()
        self.log_τ = nn.Parameter(torch.tensor([0.0]))
        self.enc = nn.Sequential(nn.Linear(D, 200), nn.ReLU(), nn.Linear(200, 2 * M))
        self.dec = nn.Sequential(nn.Linear(M, 200), nn.ReLU(), nn.Linear(200, D))

    def forward(self, x):
        N, D = x.shape
        z_enc = self.enc(x).reshape(N, 2, -1)
        μ = z_enc[:, 0]
        log_σ = z_enc[:, 1]
        if self.training:
            z = μ + torch.randn_like(μ) * (0.5 * log_σ).exp()
        else:
            # TODO why not sample at test time?
            z = μ
        x_rec = self.dec(z)
        log_τ = self.log_τ
        return μ, log_σ, x_rec, log_τ


def loss_vae(x, x_rec, μ, log_σ, log_τ, β):
    # τ = log_τ.exp()
    # loss_rec = (x - x_rec) ** 2 / τ ** 2 + τ
    # loss_rec = (x - x_rec) ** 2
    # loss_rec = torch.mean(torch.sum(loss_rec, dim=-1) / τ ** 2 + τ)
    normal = MultivariateNormal(x, log_τ.exp() ** 2 * torch.eye(D))
    loss_rec = - normal.log_prob(x_rec).mean()
    loss_kl = 0.5 * (log_σ.exp() - log_σ - 1 + μ ** 2)
    loss_kl = torch.mean(torch.sum(loss_kl, dim=-1))
    return {
        "loss": loss_rec + loss_kl / β,
        "loss-rec": loss_rec,
        "loss-kl": loss_kl,
    }


def train(β):
    def train_step():
        opt.zero_grad()
        idxs = torch.randperm(N_PTS)[:B]
        x = data[idxs].to(DEVICE)
        μ, log_σ, x_rec, log_τ = vae(x)
        losses = loss_vae(x, x_rec, μ, log_σ, log_τ, β)
        losses["loss"].backward()
        opt.step()
        return {**losses, "x": x, "x-rec": x_rec, "log-τ": log_τ}

    vae.train()
    for i in tqdm(range(ITERS)):
        output = train_step()
        if i % 1000 == 0:
            plot_rec(output["x"].detach(), output["x-rec"].detach())
            print("{:5d} · loss = {:.5f} · loss-rec = {:.3f} · loss-kl = {:.3f} · τ = {:.3f}".format(
                i,
                output["loss"].item(),
                output["loss-rec"].item(),
                output["loss-kl"].item(),
                output["log-τ"].detach().exp().mean(),
            ))


def sample():
    x1 = torch.linspace(XMIN, XMAX, 25)
    x2 = torch.linspace(XMIN, XMAX, 25)
    x = torch.meshgrid(x1, x2, indexing="ij")
    x = torch.stack(x, dim=2).reshape(-1, 2).to(DEVICE)
    vae.eval()
    with torch.no_grad():
        μ, log_σ, x_rec, log_τ = vae(x)
        x_rec = x_rec.cpu()
    return {
        "x": x,
        "x-rec": x_rec,
        "z-mu": μ,
    }


β = 1.0
vae = VAE()
vae.to(DEVICE)
opt = torch.optim.Adam(vae.parameters(), lr=8e-4)
train(β)
output = sample()
plot_rec(output["x"], output["x-rec"])
# pdb.set_trace()
