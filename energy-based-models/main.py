import pdb

from matplotlib import pyplot as plt
from sklearn.datasets import make_blobs, make_circles

import streamlit as st
import torch
import tqdm


torch.manual_seed(1338)


class MLP(torch.nn.Module):
    def __init__(self):
        super().__init__()
        h = 32
        self.network = torch.nn.Sequential(
            torch.nn.Linear(2, h),
            torch.nn.ELU(),
            torch.nn.Linear(h, h),
            torch.nn.ELU(),
            torch.nn.Linear(h, h),
            torch.nn.ELU(),
            torch.nn.Linear(h, 1),
        )

    def forward(self, x):
        return self.network(x)


def criterion_score_mathcing(network, inputs):
    n_samples, n_dimensions = inputs.shape

    inputs.requires_grad_(True)
    log_p = -network(inputs).sum()
    grad1 = torch.autograd.grad(log_p, inputs, create_graph=True, retain_graph=True)[0]
    loss1 = torch.norm(grad1, dim=-1) ** 2 / 2.0

    loss2 = torch.zeros(n_samples, device=inputs.device)
    for i in tqdm.tqdm(range(n_dimensions)):
        grad2 = torch.autograd.grad(grad1[:, i].sum(), inputs, create_graph=True, retain_graph=True)[0][:, i]
        loss2 = loss2 + grad2

    loss = loss1 + loss2
    loss = loss.mean()
    return loss


def plot_pred(mlp, X):
    x_min, y_min = X.min(axis=0)
    x_max, y_max = X.max(axis=0)
    δ = 1.0
    step = 0.2

    x1, x2 = torch.meshgrid(
        [
            torch.arange(x_min - δ, x_max + δ, step),
            torch.arange(y_min - δ, y_max + δ, step),
        ]
    )
    grid_x = torch.stack((x1.flatten(), x2.flatten())).t()

    mlp.eval()
    with torch.no_grad():
        grid_y = mlp(grid_x)
    y = grid_y.view(x1.shape).numpy()

    fig, ax = plt.subplots()
    ax.pcolormesh(x1, x2, y)
    ax.scatter(X[:, 0], X[:, 1], color="red")
    return fig


def main():
    NUM_EPOCHS = 1024
    X, y = make_blobs(n_samples=400, random_state=1338)
    mlp = MLP()

    X_tensor = torch.tensor(X).float()
    optimizer = torch.optim.Adam(mlp.parameters(), lr=0.001)

    for e in range(NUM_EPOCHS):
        if e % 256 == 0:
            st.text(f"epoch: {e}")
            fig = plot_pred(mlp, X)
            st.pyplot(fig)

        loss = criterion_score_mathcing(mlp, X_tensor)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        print(e, loss.detach())

    st.text(e)
    fig2 = plot_pred(mlp, X)
    st.pyplot(fig2)

    st.code(-mlp(X_tensor))


if __name__ == "__main__":
    main()
