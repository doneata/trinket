# This notebook is an exploration of problem 15.2 from the EE263 course:
# https://see.stanford.edu/materials/lsoeldsee263/homeworkProblems.pdf
#
# We have a measurement y, which corresponds to a linear combination of inputs x:
# 
#   y = A x
#
# We want to estimate x, but y might be perturbed by additive noise δy.
# According to the theory we can bound the relative error in the estimated solution:
#
#   ‖δx‖ / ‖x‖ ≤ κ ‖δy‖ / ‖y‖
#
# That is: the relative error in solution x ≤ condition number × the relative error in data.
# The goal of this exercise is to find x and δx for which the inequaility is tight.

import pdb
import numpy as np
from scipy.linalg import norm, svd

# Select a random measurment matrix (an invertible square matrix).
N = 4
X = np.random.randn(N, N) + 1e-3 * np.eye(N)
A = X @ X.T

# Compute the condition number.
U, σ, V = svd(A)
κ = σ.max() / σ.min()
print("κ =", κ)

# Some examples of randomly chosen input data, x and δx:
x = np.random.randn(4)
y = A @ x

δx = 1e-3 * np.random.randn(4) 
δy = A @ δx

# print(x, δx)
# print(y, δy)
print(norm(δx) / norm(x), κ * norm(δy) / norm(y))

# Based on my calculations the unlucky case is when:
#
# - x  is in the direction of the input vector corresponding to the largest singular value
# - δx is in the direction of the input vector corresponding to the smallest singular value
#
# or, alternatively,
#
# - y  is in the direction of the output vector corresponding to the largest singular value
# - δy is in the direction of the output vector corresponding to the smallest singular value.
#
# Intuitively, this happens because x = A⁻¹ y and
# the error, being in the direction of the smallest singular value, gets amplified the most.
x = V[0]
y = A @ x

δx = 1e-3 * V[-1]
δy = A @ δx

print(norm(δx) / norm(x), κ * norm(δy) / norm(y))
pdb.set_trace()
