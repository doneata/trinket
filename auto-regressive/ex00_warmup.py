import pdb

import numpy as np

from collections import Counter

from sklearn.model_selection import train_test_split

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim


SEED = 0
NR_CLASSES = 100


def sample_data():
    count = 10000
    rand = np.random.RandomState(SEED)
    a = 0.3 + 0.10 * rand.randn(count)
    b = 0.8 + 0.05 * rand.randn(count)
    mask = rand.rand(count) < 0.5
    samples = np.clip(a * mask + b * (1 - mask), 0.0, 1.0)
    return np.digitize(samples, np.linspace(0.0, 1.0, NR_CLASSES))


def split_data(data):
    split = lambda d: train_test_split(d, train_size=0.8, test_size=0.2, shuffle=False)
    tr, te = split(data)
    tr, va = split(tr)
    return tr, va, te


def estimate(data):
    counter = Counter(data.numpy())
    hist = [counter[i] for i in range(NR_CLASSES)]
    hist = np.array(hist)
    hist = hist / hist.sum()
    hist = torch.tensor(hist)
    return hist


class CategoricalModel(nn.Module):
    def __init__(self, nr_classes):
        super(CategoricalModel, self).__init__()
        self.theta = nn.Parameter(torch.zeros(nr_classes))
        self.log_softmax = nn.LogSoftmax()

    def forward(self):
        return self.log_softmax(self.theta)


def main():
    data = sample_data()
    tr_data, va_data, te_data = split_data(data)

    tr_data = torch.tensor(tr_data)
    va_data = torch.tensor(va_data)
    te_data = torch.tensor(te_data)

    n_tr = len(tr_data)

    model = CategoricalModel(NR_CLASSES)
    optimizer = optim.SGD(model.parameters(), lr=0.1)
    criterion = nn.NLLLoss()
    batch_size = 32

    def evaluate(output, data, detach=False):
        output = output[None].repeat(len(data), 1)
        loss = criterion(output, data)
        if detach:
            loss = loss.detach().numpy()
        return loss

    def train1(batch):
        optimizer.zero_grad()
        output = model()
        loss = evaluate(output, batch)
        loss.backward()
        optimizer.step()
        return loss.detach().numpy()

    hist = estimate(tr_data)
    print(evaluate(hist, va_data, detach=True))
    pdb.set_trace()

    for i in range(5000):
        idxs = torch.randint(high=n_tr, size=(batch_size,))
        batch = tr_data[idxs]
        tr_loss = train1(batch)
        va_loss = evaluate(model(), va_data, detach=True)
        print(i, tr_loss, va_loss)

    pdb.set_trace()


if __name__ == "__main__":
    main()
