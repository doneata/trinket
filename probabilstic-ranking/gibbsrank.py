import pdb

import scipy.io as sio

import scipy.linalg
import numpy as np

from tqdm import tqdm


def eq(p, players):
    mat = p == players
    mat = mat[:, np.newaxis]
    mat = mat.astype(np.float)
    return mat


def gibbs_sample(G, M, num_iters):
    """
    - G is an N × 2 matrix containing the N matches; the winner comes first.
    - M is the number of players.
    """
    # number of games
    N = G.shape[0]
    # Array containing mean skills of each player, set to prior mean
    w = np.zeros((M, 1))
    # Array that will contain skill samples
    skill_samples = np.zeros((M, num_iters))
    # Array containing skill variance for each player, set to prior variance
    pv = 0.5 * np.ones(M)

    # Number of iterations of Gibbs
    for i in (range(num_iters)):
        # Sample performance given differences in skills and outcomes
        t = np.zeros((N, 1))
        for g in range(N):
            # Difference in skills
            s = w[G[g, 0]] - w[G[g, 1]]
            # Sample performance
            t[g] = s + np.random.randn()
            # Rejection step
            while t[g] < 0:
                # Resample if rejected
                t[g] = s + np.random.randn()

        # Jointly sample skills given performance differences
        m = np.zeros((M, 1))
        for p in range(M):
            # TODO: COMPLETE THIS LINE
            # m[p] = sum(t[g] * (int(p == G[g, 0]) - int(p == G[g, 1])) for g in range(N))
            m[p] = np.sum(t * (eq(p, G[:, 0]) - eq(p, G[:, 1])))

        # Container for sum of precision matrices (likelihood terms)
        iS = np.zeros((M, M))

        for g in range(N):
            # TODO: Build the iS matrix
            iS[G[g, 0], G[g, 0]] += 1
            iS[G[g, 1], G[g, 1]] += 1
            iS[G[g, 0], G[g, 1]] -= 1
            iS[G[g, 1], G[g, 0]] -= 1

        # Posterior precision matrix
        iSS = iS + np.diag(1.0 / pv)

        # Use Cholesky decomposition to sample from a multivariate Gaussian

        # Cholesky decomposition of the posterior precision matrix
        iR = scipy.linalg.cho_factor(iSS)
        # Uses Cholesky factor to compute inv(iSS) @ m
        mu = scipy.linalg.cho_solve(iR, m, check_finite=False)

        # Sample from N(mu, inv(iSS))
        w = mu + scipy.linalg.solve_triangular(
            iR[0], np.random.randn(M, 1), check_finite=False
        )
        skill_samples[:, i] = w[:, 0]

    return skill_samples


def load_data():
    # load data
    data = sio.loadmat("tennis_data.mat")
    # Array containing the names of each player
    W = data["W"]
    # loop over array to format more nicely
    for i, player in enumerate(W):
        W[i] = player[0]
    # Array of size num_games x 2. The first entry in each row is the winner of game i, the second is the loser
    G = data["G"] - 1
    return G, W


if __name__ == "__main__":
    np.random.seed(0)
    G, W = load_data()
    # Number of players
    M = W.shape[0]
    # Number of Games
    N = G.shape[0]
    # number of iterations
    num_iters = 1100
    # perform gibbs sampling, skill samples is an num_players x num_samples array
    skill_samples = gibbs_sample(G, M, num_iters)
