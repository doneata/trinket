import pdb

from collections import Counter

import torch
import numpy as np

import pyro
import pyro.distributions as dist

from pyro.infer import SVI, Trace_ELBO, Predictive
from pyro.infer.autoguide import guides
from pyro.optim import Adam

from gibbsrank import load_data


def model(games, num_players, obs=None):
    λ = pyro.param("λ", torch.tensor(1.0))
    # λ = 1.0

    # prior
    μ = torch.zeros(num_players)
    σ = 4 * torch.ones(num_players)

    i, j = games.T
    num_games = len(games)

    # skills
    w = pyro.sample("w", dist.Normal(μ, σ))

    with pyro.plate("data", num_games):
        # perfromance per game
        noise = dist.Normal(torch.zeros(num_games), λ * torch.ones(num_games))
        ε = pyro.sample("eps", noise)
        # p1 = pyro.sample("p1", dist.Normal(w[i], 1))
        # p2 = pyro.sample("p2", dist.Normal(w[j], 1))

        winner = dist.Bernoulli(torch.sigmoid(w[i] - w[j] + ε))
        # winner = dist.Bernoulli(torch.sigmoid(p1 - p2))
        return pyro.sample("y", winner, obs=obs)


# set up the optimizer
optimizer = Adam({"lr": 0.04})

# setup the inference algorithm
guide = guides.AutoNormal(model)
svi = SVI(model, guide, optimizer, loss=Trace_ELBO())

games, players = load_data()
games = torch.tensor(games).long()
num_players = len(players)

# do gradient steps
for it in range(5000):
    obs = torch.ones(len(games))
    nll = svi.step(games, num_players, obs)
    if it % 100 == 0:
        locs = pyro.param("AutoNormal.locs.w").data.numpy()
        scales = pyro.param("AutoNormal.scales.w").data.numpy()
        idx = locs.argmax()
        print(
            "{:5d} | {:6.3f} | {:+.3f} ± {:.2f} {:s}".format(
                it, nll, locs[idx], scales[idx], players[idx, 0]
            )
        )

selected_players = [
    "Novak-Djokovic",
    "Roger-Federer",
    "Rafael-Nadal",
    "Andy-Murray",
    "Jo-Wilfried-Tsonga",
    "Milos-Raonic",
]
n = len(selected_players)
selected_ids = [np.where(players == p)[0].item() for p in selected_players]
matches = [
    (selected_ids[i], selected_ids[j]) for i in range(n) for j in range(i + 1, n)
]


def guide1(games, num_players):
    num_games = len(games)

    locs = pyro.param("AutoNormal.locs.w")
    scales = pyro.param("AutoNormal.scales.w")
    pyro.sample("w", dist.Normal(locs, scales))

    λ = pyro.param("λ")
    noise = dist.Normal(torch.zeros(num_games), λ * torch.ones(num_games))
    pyro.sample("eps", noise)


pred = Predictive(model, guide=guide1, num_samples=1000)
results = pred(torch.tensor(matches).long(), num_players)

locs = pyro.param("AutoNormal.locs.w").data.numpy()
scales = pyro.param("AutoNormal.scales.w").data.numpy()

skill_samples = results["w"].data.numpy().T

for p in selected_ids:
    print(f"{players[p, 0]:20s} {locs[p]:+.3f} ± {scales[p]:.2f}")

for (i, j), result in zip(matches, results["y"].T):
    w = 100 * result.mean()
    s = 100 * (skill_samples[i] > skill_samples[j]).mean()
    # s = result.std()
    print(f"{players[i, 0]:20s} wins over {players[j, 0]:20s}: {w:6.2f}% | {s:6.2f}%")

print(pyro.param("λ"))
pdb.set_trace()
