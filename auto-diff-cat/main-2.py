from typing_extensions import Protocol


class Category(Protocol):
    @staticmethod
    def id(): ...

    @staticmethod
    def o(f, g): ...


class Monoidal(Protocol):
    @staticmethod
    def x(f, g): ...


class Func(Category, Monoidal):
    def __init__(self, func):
        self.func = func

    def eval(self, arg):
        return self.func(arg)

    @staticmethod
    def id() -> "Func":
        return Func(lambda a: a)

    @staticmethod
    def o(f: "Func", g: "Func") -> "Func":
        def func(a):
            return f.eval(g.eval(a))
        return Func(func)

    @staticmethod
    def x(f: "Func", g: "Func"):
        def func(ab):
            a, b = ab
            return (f.eval(a), g.eval(b))
        return Func(func)


def linearD(f):
    return Diff(lambda a: (f(a), f))


class Diff(Category):
    def __init__(self, func):
        self.func = func

    def eval(self, arg):
        return self.func(arg)

    @staticmethod
    def id():
        return linearD(lambda a: a)

    @staticmethod
    def o(f: "Diff", g: "Diff") -> "Diff":
        def func(a):
            b, f1 = f.eval(a)
            c, g1 = g.eval(b)
            return c, lambda a: f1(g1(a))
        return Diff(func)


interp = "diff"
d = lambda a: 2 * a
i = lambda a: a + 1

if interp == "func":
    c = Func  # type: Category
    double = Func(d)
    inc = Func(i)
else:
    c = Diff  # type: Category
    double = linearD(d)
    inc = linearD(i)

print(c.o(double, inc).eval(10)) 
