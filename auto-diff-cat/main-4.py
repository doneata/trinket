import pdb
from dataclasses import dataclass
from typing import Callable


@dataclass
class Expr:
    def eval(self, a):
        raise NotImplemented

    def diff(self, a):
        raise NotImplemented


@dataclass
class Linear(Expr):
    f: Callable

    def eval(self, a):
        return self.f(a)

    def diff(self, a, x):
        return self.f(x)


class Mul(Expr):
    def eval(self, ab):
        a, b = ab
        return a * b

    def diff(self, ab, xy):
        a, b = ab
        x, y = xy
        return b * x + a * y


class Dup(Expr):
    def eval(self, a):
        return (a, a)

    def diff(self, a, x):
        return (x, x)


@dataclass
class Compose(Expr):
    g: Expr
    f: Expr

    def eval(self, a):
        return self.g.eval(self.f.eval(a))

    def diff(self, a, x):
        # K.compose g.diff and f.diff
        return self.g.diff(self.f.eval(a), self.f.diff(a, x))


# Vocabulary
compose = Compose
mul = Mul
# dup = (Id(), Id())
dup = Dup


sqr = lambda: compose(mul(), dup())
fth = compose(sqr(), sqr())
# fth = sqr()

print(fth)
print(fth.eval(10))
print(fth.diff(10, 1))

# Example; take Herman's function
# https://www.kamperh.com/notes/kamper_backprop22.pdf
