"""Implementing Conal Elliott's The Simple Essence of Automatic Differentiation."""
import pdb

from functools import partialmethod
from typing import Any, Callable
from typing_extensions import Protocol


class Category(Protocol):
    def id(self):
        ...

    def o(self, f, g):
        ...


class Cartesian(Protocol):
    def exl(self, prod):
        ...

    def exr(self, prod):
        ...

    def fork(self, f, g):
        ...


class Monoidal(Protocol):
    def cross(self, f, g):
        ...


class Numerical(Protocol):
    def negate(self):
        ...

    def add(self):
        ...

    def mul(self):
        ...


class Func:
    def id(self):
        return lambda a: a

    def o(self, f, g):
        return lambda a: f(g(a))

    def cross(self, f, g):
        def func(ab):
            a, b = ab
            return (f(a), g(b))

        return func


class Cont:
    pass


class Dual:
    pass


class Diff(Category, Cartesian, Numerical):
    def __init__(self, c):
        self.c = c

    def linear(self, f):
        return lambda a: (f(a), f)

    def id(self):
        return lambda a: (a, lambda x: x)

    def o(self, f, g):
        def func(a):
            b, g1 = g(a)
            c, f1 = f(b)
            return c, f1(g1(a))

        return func

    def cross(self, f, g):
        pass

    def fork(self, f, g):
        def func(a):
            b, f1 = g(a)
            c, g1 = g(a)
            return (b, c), lambda a: (f1(a), g1(a))

        return func

    def exl(self, ab):
        a, b = ab
        return a, lambda xy: xy[0]

    def exr(self, ab):
        a, b = ab
        return b, lambda xy: xy[1]

    def dup(self, a):
        return (a, a), lambda x: (x, x)

    def mul(self, ab):
        a, b = ab
        return a * b, lambda xy: xy[0] * b + xy[1] * a

    def negate(self):
        return self.linear(lambda val: -val)


# inc = linear(lambda a: a + 1)
# double = linear(lambda a: 2 * a)
# print((inc |o| double)(10))
# print((double |o| inc)(10))
# TODO 
D = Diff(Func())
print((D.o(D.mul, D.dup))(10))
