import pdb
from dataclasses import dataclass
from typing import Callable


# Syntax (vocabulary)
#
# The paper defines a vocabulary, but it's implicit; it reaches rewrites a
# Haskell expression into this vocabulary automatically through Conal's CCC
# plugin.


@dataclass
class Expr:
    def __lshift__(self, other):
        return Compose(self, other)

    def __rshift__(self, other):
        return Compose(other, self)

    def __matmul__(self, other):
        return Cross(self, other)


@dataclass
class Id(Expr):
    pass


@dataclass
class Compose(Expr):
    f: Expr
    g: Expr


@dataclass
class Cross(Expr):
    f: Expr
    g: Expr


@dataclass
class Exl(Expr):
    pass


@dataclass
class Exr(Expr):
    pass


@dataclass
class Dup(Expr):
    pass


@dataclass
class Scale(Expr):
    pass


@dataclass
class Negate(Expr):
    pass


@dataclass
class Add(Expr):
    pass


@dataclass
class Mul(Expr):
    pass


id_ = Id()
# compose = lambda fg: Compose(fg[0], fg[1])

# cross = lambda fg: Cross(fg[0], fg[1])

exl = Exl()
exr = Exr()
dup = Dup()

scale = Scale()
negate = Negate()
add = Add()
mul = Mul()


# Semnatic domains


class Semantic:
    # Category
    def id_(self, a):
        raise NotImplemented

    def compose(self, f, g):
        raise NotImplemented

    # Monoidal
    def cross(self, f, g):
        raise NotImplemented

    # Cartesian
    def exl(self, ab):
        raise NotImplemented

    def exr(self, ab):
        raise NotImplemented

    def dup(self, a):
        raise NotImplemented

    # Cocartesian
    def inl(self, a):
        raise NotImplemented

    def inr(self, b):
        raise NotImplemented

    def jam(self, ab):
        raise NotImplemented

    # Numeric
    def scale(self, a):
        # a → a `k` a
        raise NotImplemented

    def negate(self, a):
        raise NotImplemented

    def add(self, ab):
        raise NotImplemented

    def mul(self, ab):
        raise NotImplemented

    # Helpers:
    # - `fork` and `unfork` use the `Cartesian` implementations
    # - `join` and `unjoin` use the `Cocartesian` implementations
    def fork(self, f, g):
        # (a → c) × (a → d) → (a → (c × d))
        return self.compose(self.cross(f, g), self.dup)

    def unfork(self, f, g):
        # (a → (c × d)) → (a → c) × (a → d)
        return (self.compose(self.exl, h), self.compose(self.exr, h))

    def join(self, f, g):
        # (c → a) × (d → a) → (c × d) → a
        return self.compose(self.jam, self.cross(f, g))

    def unjoin(self, h):
        # ((c × d) → a) → (c → a) × (d → a)
        return (self.compose(h, self.inl), self.compose(h, self.inr))


class Func(Semantic):
    """Additive functions."""

    # Category
    def id_(self, a):
        return a

    def compose(self, f, g):
        return lambda a: f(g(a))

    def compose_unpack(self, f, g):
        return lambda a: f(*g(a))

    # Monoidal
    def cross(self, f, g):
        return lambda ab: (f(ab[0]), g(ab[1]))

    # Cartesian
    def exl(self, ab):
        return ab[0]

    def exr(self, ab):
        return ab[1]

    def dup(self, a):
        return a, a

    # Cocartesian
    def inl(self, a):
        return (a, 0)

    def inr(self, b):
        return (0, b)

    def jam(self, ab):
        a, b = ab
        return a + b
        # # TODO Clean
        # if isinstance(a, Callable) and isinstance(b, Callable):

        #     def f(x):
        #         aa = a(x)
        #         bb = b(x)
        #         if isinstance(aa, tuple) and isinstance(bb, tuple):
        #             return tuple(u + v for u, v in zip(aa, bb))
        #         else:
        #             return aa + bb

        #     return f
        # else:
        #     return a + b

    # Numeric
    def scale(self, a):
        return lambda da: a * da

    def add(self, ab):
        return ab[0] + ab[1]

    def mul(self, ab):
        return ab[0] * ab[1]


class Cont(Semantic):
    def __init__(self, ops: Semantic):
        self.ops = ops

        self.dup = ops.compose(ops.jam, ops.unjoin)
        self.jam = ops.compose_unpack(ops.join, ops.dup)
        self.scale = ops.scale

    def compose(self, f, g):
        return self.ops.compose(g, f)

    def cross(self, f, g):
        return self.ops.compose(
            self.ops.join,
            self.ops.compose(
                self.ops.cross(f, g),
                self.ops.unjoin,
            )
        )


class Dual(Semantic):
    def __init__(self, ops: Semantic):
        self.ops = ops

        self.id_ = ops.id_
        self.cross = ops.cross

        self.exl = ops.inl
        self.exr = ops.inr
        self.dup = ops.jam

        self.inl = ops.exl
        self.inr = ops.exr
        self.jam = ops.dup

        self.scale = ops.scale

        # self.add = ops.add
        # self.mul = ops.mul

    def add(self, a):
        return (a, a)

    def compose(self, f, g):
        return self.ops.compose(g, f)


F = Func()


def fn(expr: Expr) -> Callable:
    """Evaluates an expression to a Python function."""
    if isinstance(expr, Id):
        return F.id_
    elif isinstance(expr, Compose):
        return F.compose(to_func(expr.f), to_func(expr.g))
    elif isinstance(expr, Cross):
        return F.cross(to_func(expr.f), to_func(expr.g))
    elif isinstance(expr, Dup):
        return F.dup
    elif isinstance(expr, Add):
        return F.add
    elif isinstance(expr, Mul):
        return F.mul
    else:
        assert False


def ad(expr: Expr, ops: Semantic) -> Callable:
    """Evaluates an expression to its differentiation function.
    This implementation is based on the generalized automatic differentiaton algorithm
    described in Figure 6 of Conal Elliott's paper "The Simple Essence of Automatic Diffentiation".

    """
    def lineard(f, f1):
        return lambda a: (f(a), f1)

    # Category
    if isinstance(expr, Id):
        return lineard(F.id_, ops.id_)
    elif isinstance(expr, Compose):

        def compose1(a):
            b, g1 = ad(expr.g, ops)(a)
            c, f1 = ad(expr.f, ops)(b)
            return c, ops.compose(f1, g1)

        return compose1

    # Monoidal
    elif isinstance(expr, Cross):

        def cross1(ab):
            a, b = ab
            c, f1 = ad(expr.f, ops)(a)
            d, g1 = ad(expr.g, ops)(b)
            return (c, d), ops.cross(f1, g1)

        return cross1

    # Cartesian
    elif isinstance(expr, Dup):
        return lineard(F.dup, ops.dup)

    # Numeric
    elif isinstance(expr, Add):
        return lineard(F.add, ops.add)
    elif isinstance(expr, Mul):

        def mul1(ab):
            a, b = ab
            sa = ops.scale(a)
            sb = ops.scale(b)
            return F.mul(ab), ops.join(sb, sa)

        return mul1
    else:
        print(expr)
        assert False


# Example · x² + y²
sqr = dup >> mul
mag = sqr @ sqr >> add

# mag_func = fn(mag)
# mag_diff = ad(mag, F)

# print(mag)
# print(mag_func((3, 4)))
# print(mag_diff((3, 4))[1]((1, 0)))
# print(mag_diff((3, 4))[1]((0, 1)))

print(ad(mag, Dual(F))((3, 4))[1](1))

# Differentiate the square function `sqr` using automatic differentiation
# implementation `ad` instatated with the continuation over functions
# semantics.
# sqr_diff_cont = to_diff(sqr, Cont(F))
# print(sqr_diff_cont(3)[1](1))

# print(ad(mag, F)((3, 4))[1]((1, 1)))
