import pdb

from abc import ABCMeta, abstractmethod
from math import exp

from toolz import compose, curry, identity


const = curry(lambda v, _: v)


def fork(f, g):
    return lambda a: (f(a), g(a))


def compose_prod(f, g):
    def h(a):
        return f(a) * g(a)
    return h


# class D:
#     def __call__(self, a) -> Tuple[b, LinMap[a, b]]:
#         pass



class Func(metaclass=ABCMeta):
    @abstractmethod
    def forward(self, a):
        pass        

    def backward(self, a):
        _, df = self.backward1(a)
        return df

    def backward1(self, a):
        b = self.forward(a)
        return b, self.backward(a)


class Compose(Func):
    def __init__(self, g: Func, f: Func):
        self.g = g
        self.f = f

    def forward(self, a):
        return compose(self.g.forward, self.f.forward)(a)

    def backward(self, a):
        df = self.f.backward(a)
        dg = self.g.backward(self.f.forward(a))
        return compose_prod(dg, df)

    def __str__(self):
        return str(self.g) + " ○ " + str(self.f)


class Id(Func):
    def __init__(self):
        self.forward = identity
        self.backward = const(1)


class Fork(Func):
    def __init__(self, f: Func, g: Func):
        self.forward = fork(f.forward, g.forward)
        self.backward = fork(f.backward, g.backward)


class Exp(Func):
    def forward(self, a):
        return exp(a)

    def backward(self, a):
        return lambda x: exp(a) * x

    def __str__(self):
        return "exp"


class Inc(Func):
    def forward(self, a):
        return a + 1

    def backward(self, a):
        return lambda _: 1

    def __str__(self):
        return "(+1)"

# ε = 1e-8
# f = lambda x: exp(x) + 1
# print((f(1 + ε) - f(1)) / ε)

exprs = [
    Compose(Inc(), Exp()),
    Compose(Exp(), Inc()),
    Compose(Compose(Exp(), Inc()), Exp()),
]
for expr in exprs:
    x = 1
    print(expr)
    print(expr.forward(x))
    print(expr.backward(x)(x))
    print()

import torch
from torch.autograd import grad, Variable

x = Variable(torch.tensor([1.0]), requires_grad=True)
y = Variable(x + 1.0, requires_grad=True)
z = torch.exp(y)
print(grad(z, x))
print(grad(y, x))
print(grad(z, y))
import pdb; pdb.set_trace()


