import argparse
import pdb
import numpy as np
import sys

from itertools import cycle

from matplotlib import pyplot as plt

import torch
import torch.distributions as ds

from torch import nn
from torch.nn import functional as F
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import TensorDataset, DataLoader


def sample_data(count):
    rand = np.random.RandomState(0)
    a = [[-1.5, 2.5]] + rand.randn(count // 3, 2) * 0.2
    b = [[1.5, 2.5]] + rand.randn(count // 3, 2) * 0.2
    c = np.c_[
        2 * np.cos(np.linspace(0, np.pi, count // 3)),
        -np.sin(np.linspace(0, np.pi, count // 3)),
    ]
    c += rand.randn(*c.shape) * 0.2
    data_x = np.concatenate([a, b, c], axis=0)
    data_y = np.array([0] * len(a) + [1] * len(b) + [2] * len(c))
    perm = rand.permutation(len(data_x))
    return data_x[perm], data_y[perm]


# GMM-CDF flow

def neg_log_likelihood(X, params):
    pi, mu, sigma = params
    prob = ds.Normal(mu, sigma).log_prob(X).exp()
    return - (pi * prob).sum(dim=1).log().mean()


def criterion(params, data):
    tot_nll = 0
    for i, theta in enumerate(params):
        tot_nll += neg_log_likelihood(data[:, i].unsqueeze(1), theta)
    return tot_nll / len(params)


def flow(model, data):
    params = model(data)
    z = []
    for i, theta in enumerate(params):
        X = data[:, i].unsqueeze(1)
        pi, mu, sigma = theta
        zz = ds.Normal(mu, sigma).cdf(X) * pi
        zz = zz.sum(1)
        z.append(zz)
    return torch.stack(z).t()


class FlowNet(nn.Module):
    def __init__(self, K):
        super(FlowNet, self).__init__()
        self.α = nn.Parameter(torch.randn(1, K))
        self.μ = nn.Parameter(torch.randn(1, K))
        self.log_σ = nn.Parameter(torch.randn(1, K))
        self.Θ = {i + "_" + j: nn.Parameter(torch.randn(1, K))
            for i in "w b".split()
            for j in "α μ log_σ".split()
        }
        for k, v in self.Θ.items():
            self.register_parameter(k, v)
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        # Parameters of the first GMM
        π1 = self.softmax(self.α)
        μ1 = self.μ
        σ1 = self.log_σ.exp()
        params1 = π1, μ1, σ1
        # Parameters of the second GMM
        x1 = x[:, 0].unsqueeze(1)
        π2 = self.softmax(x1 * self.Θ["w_α"] + self.Θ["b_α"])
        μ2 = x1 * self.Θ["w_μ"] + self.Θ["b_μ"]
        σ2 = (x1 * self.Θ["w_log_σ"] + self.Θ["b_log_σ"]).exp()
        params2 = π2, μ2, σ2
        return params1, params2

# Real NVP flow

class RNVP(nn.Module):
    def __init__(self, *args):
        super(RNVP, self).__init__()
        self.linear = torch.nn.ModuleList([nn.modules.Linear(1, 1, bias=True) for _ in range(2)])

    def coupling_layer(self, X, i):
        def get_dim(X, i, j):
            if i % 2 == j:
                return X[:, :1]
            else:
                return X[:, 1:]
        A = self.linear[i](get_dim(X, i, 0))
        Y = torch.cat((A.exp() * get_dim(X, i, 1), get_dim(X, i, 0)), dim=1)
        if i % 2 == 0:
            Y = torch.index_select(Y, 1, torch.LongTensor([1, 0]))
        logdet = A.mean()
        return Y, logdet

    def forward(self, XX):
        Z1, logdet1 = self.coupling_layer(XX, 0)
        Z2, logdet2 = self.coupling_layer(Z1, 1)
        Z3 = torch.sigmoid(Z2)
        logdet3 = Z3.log().mean() + (1 - Z3).log().mean()
        return Z3, - (logdet1 + logdet2 + logdet3) / 2


N = 100_000  # number of sample points
K = 12  # number of gaussian components
MODEL_PATH = 'model.pth'


def train(Model, criterion):
    data, labels = sample_data(N)
    data = torch.Tensor(data)

    tr_n = N // 100 * 80
    tr_data = TensorDataset(data[:tr_n])
    te_data = TensorDataset(data[tr_n:])

    tr_data_loader = DataLoader(tr_data, batch_size=16)
    te_data_loader = DataLoader(te_data, batch_size=16)

    model = Model(K)
    optimizer = torch.optim.Adam(model.parameters(), lr=0.05)
    scheduler = StepLR(optimizer, step_size=500, gamma=0.1)

    def train_function(batch):
        model.train()
        batch = batch[0]
        optimizer.zero_grad()
        params = model(batch)
        loss = criterion(params, batch)
        loss.backward()
        optimizer.step()
        return loss.item()

    def evaluate_function(batch):
        model.eval()
        batch = batch[0]
        params = model(batch)
        return criterion(params, batch).detach()

    tr_loss = 0
    is_checkpoint = lambda i: i % 100 == 0

    for i, batch in enumerate(cycle(tr_data_loader)):
        batch_loss = train_function(batch)
        tr_loss += batch_loss
        if is_checkpoint(i):
            te_losses = [evaluate_function(b).numpy() for b in te_data_loader]
            te_loss = np.mean(te_losses)
            print("{:5d} {:.3f} {:.3f}".format(i, tr_loss / (i + 1), te_loss))
        if (i + 1) % 5000 == 0:
            print(scheduler)
            scheduler.step()
        if i > 15000:
            break


    torch.save(model.state_dict(), MODEL_PATH)


def viz(Model):
    model = Model(K)
    model.load_state_dict(torch.load(MODEL_PATH))
    model.eval()

    data, labels = sample_data(N)
    data = torch.Tensor(data)

    fig, axes = plt.subplots(2, 2)

    # Plot samples
    d = data.numpy()
    for label in [0, 1, 2]:
        idxs = labels == label
        axes[0, 0].scatter(d[idxs, 0], d[idxs, 1])

    # Plot projected samples
    z = flow(model, data)
    d = z.detach().numpy()
    for label in [0, 1, 2]:
        idxs = labels == label
        axes[0, 1].scatter(d[idxs, 0], d[idxs, 1])

    # Plot density in the original space
    x1, x2 = torch.meshgrid([torch.arange(-4, 4, 0.1), torch.arange(-4, 4, 0.1)])
    grid_x = torch.stack((x1.flatten(), x2.flatten())).t()

    shape = x1.shape

    def neg_log_likelihood1(X, params):
        pi, mu, sigma = params
        prob = ds.Normal(mu, sigma).log_prob(X).exp()
        return - (pi * prob).sum(dim=1).log()

    params1, params2 = model(grid_x)
    g1 = neg_log_likelihood1(grid_x[:, 0].unsqueeze(1), params1)
    g2 = neg_log_likelihood1(grid_x[:, 1].unsqueeze(1), params2)
    gg = - (g1 + g2) / 2
    gg = gg.exp()

    axes[1, 0].pcolormesh(x1, x2, gg.view(*shape).detach().numpy())

    # Plot deformed grid
    grid_z = flow(model, grid_x)
    z1 = grid_z[:, 0].view(*shape).detach().numpy()
    z2 = grid_z[:, 1].view(*shape).detach().numpy()

    for i in range(shape[0]):
        axes[1, 1].plot(z1[i, :], z2[i, :], color="C0")
    for i in range(shape[1]):
        axes[1, 1].plot(z1[:, i], z2[:, i], color="C0")

    plt.show()


def main():
    parser = argparse.ArgumentParser(description='Flows in 2D')
    parser.add_argument('-t', '--todo', choices={'train', 'viz'})
    parser.add_argument('-m', '--model', choices={'cdf', 'rnvp'})
    args = parser.parse_args()

    if args.model == 'cdf':
        Model = FlowNet
    elif args.model == 'rnvp':
        Model = RNVP
        def criterion(*args):
            return args[0][1]

    if args.todo == 'train':
        train(Model, criterion)
    elif args.todo == 'viz':
        viz(Model)
        train(args.model)


if __name__ == '__main__':
    main()
